// There's probably a nicer data structure for this, especially if efficiency is a concern, 
// but this is easy enough to work with.
type TBiome = boolean[][];

type TEvolver = (biome: Readonly<TBiome>) => TBiome;