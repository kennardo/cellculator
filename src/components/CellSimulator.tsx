import * as React from 'react';

import { createBiome } from '../util/createBiome';
import { evolver } from '../util/evolver';
import { evolveRandom } from '../util/evolveRandom';
import { toggleAt } from '../util/toggleAt';
import { Button } from './Button';
import { Biome } from './Biome';

type TProps = {
  width?: number;
  height?: number;
  evolver?: TEvolver;
};

type TState = {
  biome?: TBiome;
  playInterval?: number;
};

const defaultProps: TProps = {
  width: 10,
  height: 10,
  evolver,
};

class CellSimulator extends React.Component<TProps, TState> {
  static defaultProps = defaultProps;
  
  constructor(props: TProps) {
    super(props);
    const { width, height } = props;
    this.state = { biome: createBiome(width, height) };
  }

  componentWillUnmount() {
    clearInterval(this.state.playInterval);
  }

  evolve = () => {
    const { evolver } = this.props;
    const { biome } = this.state;
    this.setState({ biome: evolver(biome) });
  }
  
  reset = () => {
    const { width, height } = this.props;
    this.setState({ biome: createBiome(width, height) });
  }

  randomize = () => {
    const { biome } = this.state;
    this.setState({ biome: evolveRandom(biome) });
  }

  toggleCell = (x: number, y: number) => {
    const { biome } = this.state;
    this.setState({ biome: toggleAt(biome, x, y) });
  }

  togglePlaying = () => {
    const { playInterval } = this.state;
    if(playInterval) {
      clearInterval(playInterval)
      this.setState({ playInterval: null });
    } else {
      this.setState({ playInterval: window.setInterval(this.evolve, 200) });
    }
  }

  render() {
    const { biome, playInterval } = this.state;
    const { evolve, reset, randomize, toggleCell, togglePlaying } = this;

    return <>
      <Biome biome={biome} onCellClick={toggleCell} />
      <Button onClick={evolve}>Next generation</Button>
      <Button onClick={reset}>Reset</Button>
      <Button onClick={randomize}>Randomize</Button>
      <Button onClick={togglePlaying}>{playInterval ? "Stop" : "Play"}</Button>
    </>;
  }
}

export { CellSimulator };