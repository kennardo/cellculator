import * as React from 'react';

import { CellSimulator } from './CellSimulator';

export default {
  'default': <CellSimulator />,
  '20x20': <CellSimulator width={20} height={20} />
};
