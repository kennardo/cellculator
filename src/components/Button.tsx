import styled from 'styled-components';

const Button = styled('button')`
  padding: 6px 12px;
  margin: 10px 10px 0 0;
  border: 1px solid #ddd;
  border-radius: 3px;
  background: #fafafa;
  transition: all 100ms ease-out;
  cursor: pointer;

  &:hover {
    border-color: #ccc;
    background: #eee;
  }

  &:active {
    background: #e3e3e3;
  }
`;

Button.defaultProps = {
  type: 'button'
};

export { Button };