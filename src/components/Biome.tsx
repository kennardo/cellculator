import * as React from 'react';
import styled from 'styled-components';

import { getNeighbourCount } from '../util/getNeighbourCount';

type TCellGridProps = {
  numCols: number;
  numRows: number;
};
const CellGrid = styled('div')<TCellGridProps>(({ numCols, numRows }) => ({
  width: 'max-content',
  display: 'grid',
  gridTemplateColumns: `repeat(${numCols}, 20px)`,
  gridTemplateRows: `repeat(${numRows}, 20px)`,
  background: 'white',
  boxShadow: '0 0 10px rgba(0,0,0,0.2)',
}));

type TCellGridItemProps = {
  isAlive?: boolean;
  neighbourCount?: number;
};
const CellGridItem = styled('div')<TCellGridItemProps>(({ isAlive, onClick, neighbourCount }) => ({
  background: isAlive ? 'cornflowerblue' : null,
  opacity: 0.4 + (neighbourCount / 8 * 0.6),
  transition: 'background 120ms ease-out',
  cursor: onClick ? 'pointer' : null,
}));

type TBiomeProps = {
  biome?: TBiome;
  onCellClick?: (x: number, y: number) => void;
};
const Biome = ({ biome, onCellClick }: TBiomeProps ) => 
  <CellGrid numCols={biome[0].length} numRows={biome.length}>
    {biome?.map((row, y) => (
      row.map((isAlive, x) => 
      // Having getNeighbourCount here is technically wasteful as it was already calculated
      // within the evolver
        <CellGridItem 
          isAlive={isAlive}
          key={`${x}.${y}`}
          neighbourCount={getNeighbourCount(biome, x, y)}
          onClick={() => onCellClick(x, y)}
        />
      )
    ))}
  </CellGrid>;

export { Biome };