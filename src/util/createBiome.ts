const createBiome = (width: number, height: number): TBiome => {
  return new Array(height).fill(1).map(y => 
    new Array(width).fill(1).map(x => false)
  );
};

export { createBiome };