const evolveRandom: TEvolver = rows => 
  rows?.map(row => 
    row.map(() => 
      Math.random() > 0.5
    )
  );

export { evolveRandom };