const toggleAt = (biome: Readonly<TBiome>, x: number, y: number): TBiome => {
  const biomeCopy = biome.map(row => row.slice());
  biomeCopy[y][x] = !biomeCopy[y][x];
  return biomeCopy;
};

export { toggleAt };