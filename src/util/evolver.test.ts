import { evolver } from './evolver';

const _ = false;
const X = true;

describe('a living cell', () => {
  test('with 0 live neighbours will die', () => {
    const biome = [
      [_,_,_,_],
      [_,_,_,_],
      [_,X,_,_],
      [_,_,_,_],
    ];
    const newBiome = evolver(biome);
    expect(newBiome[2][1]).toEqual(false);
  });

  test('with 1 live neighbours will die', () => {
    const biome = [
      [_,_,_,_],
      [_,_,_,_],
      [_,X,_,_],
      [X,_,_,_],
    ];
    const newBiome = evolver(biome);
    expect(newBiome[2][1]).toEqual(false);
  });

  test('with 2 live neighbours will live', () => {
    const biome = [
      [X,_,_,X],
      [_,_,_,_],
      [_,_,_,_],
      [X,_,_,_],
    ];
    const newBiome = evolver(biome);
    expect(newBiome[0][0]).toEqual(true);
  });

  test('with 3 live neighbours will live', () => {
    const biome = [
      [X,X,_,X],
      [_,_,_,_],
      [_,_,_,_],
      [X,_,_,_],
    ];
    const newBiome = evolver(biome);
    expect(newBiome[0][0]).toEqual(true);
  });

  test('with > 3 live neighbours will die', () => {
    const biome = [
      [X,X,_,X],
      [_,X,_,_],
      [_,_,_,_],
      [X,_,_,_],
    ];
    const newBiome = evolver(biome);
    expect(newBiome[0][0]).toEqual(false);
  });
});

describe('a dead cell', () => {
  test('with 3 live neighbours will live', () => {
    const biome = [
      [_,X,_,_],
      [X,X,_,_],
      [_,_,_,_],
      [_,_,_,_],
    ];
    const newBiome = evolver(biome);
    expect(newBiome[0][0]).toEqual(true);
  });
});