import { getNeighbourCount } from "./getNeighbourCount";

const _ = false;
const X = true;

describe('getNeighbourCount counts', () => {
  test('direct neighbours', () => {
    const biome = [
      [_,_,_,_],
      [X,_,_,_],
      [_,_,_,_],
      [_,_,X,_],
    ];
    expect(getNeighbourCount(biome, 1, 2)).toEqual(2);
  });
  
  test('across horizontal edges', () => {
    const biome = [
      [_,X,_,X],
      [_,_,_,_],
      [_,_,_,_],
      [_,_,_,_],
    ];
    expect(getNeighbourCount(biome, 0, 0)).toEqual(2);
  });

  test('across vertical edges', () => {
    const biome = [
      [_,_,_,_],
      [X,_,_,_],
      [_,_,_,_],
      [X,_,_,_],
    ];
    expect(getNeighbourCount(biome, 0, 0)).toEqual(2);
  });

  test('across horizontal and vertical edges', () => {
    const biome = [
      [_,_,_,_],
      [_,_,_,_],
      [_,_,_,_],
      [_,_,_,X],
    ];
    expect(getNeighbourCount(biome, 0, 0)).toEqual(1);
  });
});