import { getNeighbourCount } from "./getNeighbourCount";

const evolver: TEvolver = (biome) => 
  biome.map((rows, y) =>
    rows.map((isAlive, x) => {
      const neighbourCount = getNeighbourCount(biome, x, y);
      if(isAlive) {
        if(neighbourCount < 2) return false;
        if(neighbourCount <= 3) return true;
        if(neighbourCount >= 4) return false;
      } else {
        return neighbourCount == 3;
      }
    })
  );

export { evolver };