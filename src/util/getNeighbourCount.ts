const getNeighbourCount = (biome: Readonly<TBiome>, x: number, y: number): number => {
  // Wrap around grid edges
  const w = biome[0].length;
  const h = biome.length;
  const left  = ((x-1)+w) % w;
  const above = ((y-1)+h) % h;
  const right = (x+1) % w;
  const below = (y+1) % h;

  var aliveCount = 0;
  biome[above][left] && aliveCount++;
  biome[above][x] && aliveCount++;
  biome[above][right] && aliveCount++;
  biome[y][left] && aliveCount++;
  biome[y][right] && aliveCount++;
  biome[below][left] && aliveCount++;
  biome[below][x] && aliveCount++;
  biome[below][right] && aliveCount++;

  return aliveCount;
};

export { getNeighbourCount };