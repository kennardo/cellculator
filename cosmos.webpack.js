module.exports = (webpackConfig, _env) => {
  return { ...webpackConfig, mode: "development" };
};
